# imap-email-notifier
## Description

This python script print number of uread email messages. It uses IMAP for
connecting to your mailbox.

## Dependencies

Python 3, [python-keyring](https://pypi.python.org/pypi/keyring) (optional),
gnome-keyring (if pyton-keyring using, also you able to use any other
compatible backend)

**Packages names in Debian based distros**:
python3, python3-keyring, gnome-keyring

**Packages names in Arch Linux**:
python, python-keyring, gnome-keyring


Alternatively you can get python-keyring with
[pip](https://pypi.python.org/pypi/pip):

```shell
$ pip install keyring
```

## Usage

There are two ways to set up this script. You able to specify options in
**"Settings"** section inside [imap-email-notifier](imap-email-notifier) file.
Another way is to create config file in **~/.config/i3blocks-email/** directory.
In this way you able to have multiple instances of script.

Now keep in mind, that there are also two ways to specify your mailbox password.
One way is to past it between apostrophes in PASS line in script or config
file. **It is not secure and it is recommended for debugging only!**

Another way is to use a system keyring. In this way you should keep PASS empty
as is. To add your password to keyring run in folder with script:

```shell
$ ./imap-email-notifier --add $USER
```
where $USER is your mailbox login.

**python-keyring and compatible backend should be installed and be in $PATH.**

You also able to delete key with:
```shell
$ ./imap-email-notifier --remove $USER
```


If you want to use few instances of script in the same time, you should make
config file in directory **~/.config/imap-email-notifier/** with following
structure:
```INI
[MAIL]

HOST: imap.mail_server.com
PORT: 993
USER: my_mailbox@mail_server.com
PASS:
LABEL: messages:

```

The [MAIL] header should exist. Any options are optional and replace the same
options inside script.
The minimal config file is:
```INI
[MAIL]

HOST: imap.mail_server.com
USER: my_mailbox@mail_server.com
```

When config files is created, you able to start sript with option:
```shell
$ ./imap-email-notifier --config $CONFIG_NAME
```
Where $CONFIG_NAME is name of cinfig file without path.

After configuring you able to use script with command applet in the
[MATE desktop environment](http://mate-desktop.com/) for example.
